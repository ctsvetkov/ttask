# ttask

This project is a decision of the task given for the job application in a company **T**.

**The description of the task is:**

----
At **T** we often deal with ingesting data and converting it into target formats for consumption into different systems - source and destination systems vary from accounting systems, data-extracts to REST APIs or SOAP calls. An [example file](https://gitlab.com/ctsvetkov/ttask/-/blob/develop/data/invoices.csv) is attached and contains invoice data (amounts, identifiers etc) as well as a base64 encoded invoice image. The real files can be 2GB+.

Problem description:
We need to write a system that parses and ingests the given (large) file and has the ability to produce the two different output formats specified below.
As a user of that system I need to be able to configure or otherwise specify which of the two output formats should be produced.
The new output formats will then later on be ingested by other systems - the integrity of data and files has to stay. The later ingestion of the newly produced files is not part of this exercise.  

The two destination formats should be:
1. CSV file of the original data but split up by 'buyer'. So if there are 10 different buyers overall there should be 10 different output files. The rest of the data in the CSV should be arranged in the same way as in the input file.
2. XML file of the original data split up by 'buyer'. The invoice image should not be part of the XML data but the single invoice files should be extracted from the CSV and be placed into the file-system. The format of the XML should loosely follow the input CSV in regards to node-names etc.You can decide any changes to folder-structure etc. of the output format.

It is up to you what language you develop the solution in as long as we can see the solution running and walk through the code and output files you produced together with.

Unit tests would be appreciated.

----

## About 

This project is a standard Spring Boot application written in Java.

There are defined 6 HTTP endpoint:
* `http://localhost:8080/api/invoices/ingest`: ingest the example file ([invoices.csv](https://gitlab.com/ctsvetkov/ttask/-/blob/develop/data/invoices.csv))
* `http://localhost:8080/api/invoices`: list all ingested invoices in XML or JSON format depends on the HTTP Header `Accept`
* `http://localhost:8080/api/invoices/<InvoiceID>`: list the invoice with specified ID in XML or JSON format
* `http://localhost:8080/api/invoices/<InvoiceID>/image`: get the image of the invoice with specified ID
* `http://localhost:8080/api/invoices/csv`: list ingested invoices in CSV format

## Starting the project

To run the project use:
```sh
git clone git@gitlab.com:ctsvetkov/ttask.git
cd ./ttask
./mvnw install
./mvnw spring-boot:run

```

To trigger ingestion of the original invoice data use:
```sh
curl -X POST localhost:8080/api/invoices/ingest -d 'url=https://gitlab.com/ctsvetkov/ttask/-/raw/develop/data/invoices.csv' -v
```

## Output the invoices in required formats

### Export in CSV format
To output/ingest all the invoices in CSV format use:
```sh
curl http://localhost:8080/api/invoices/csv --output invoices.csv
```

To output all the invoices for a specific **buyer** use:
```sh
curl http://localhost:8080/api/invoices/csv\?buyer\=Traksas --output invoices.csv
curl http://localhost:8080/api/invoices/csv\?buyer\=South+African+Gold+Mines+Corp --output invoices.csv
curl http://localhost:8080/api/invoices/csv\?buyer\=Axtronics --output invoices.csv
```

In addition you can use any of the `number, buyer, supplier, status` query parameters.

For example if you want to ingest all the invoices for buyer: **South African Gold Mines Corp** with status **NEW** use:
```sh
curl http://localhost:8080/api/invoices/csv\?buyer\=South+African+Gold+Mines+Corp\&status\=NEW --output invoices.csv
```

### Export in XML format

To output/ingest all the invoices in XML format use:
```sh
curl http://localhost:8080/api/invoices -H "Accept: application/xml" -v
```

To output all the invoices for a specific **buyer** use:
```sh
curl http://localhost:8080/api/invoices\?buyer\=Axtronics -H "Accept: application/xml" -v
```

To output a single invoice by it's ID use:
```sh
curl http://localhost:8080/api/invoices/1 -H "Accept: application/xml" -v
```
