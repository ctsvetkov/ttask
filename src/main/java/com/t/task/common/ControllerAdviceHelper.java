package com.t.task.common;

import org.springframework.hateoas.mediatype.vnderrors.VndErrors;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.hateoas.Link;

/**
 * @author Tsvetan Tsvetkov <c.tsvetkov@gmail.com>
 */
public class ControllerAdviceHelper {
    /**
     * Build application/vnd.error+json ResponseEntity
     *
     * @param status
     * @param logref
     * @param message
     * @param link
     * @return ResponseEntity
     */
    public static ResponseEntity<VndErrors.VndError> toResponseEntity(HttpStatus status, String logref, String message, Link link) {
        return ResponseEntity.status(status)
            .header("content-type", "application/vnd.error+json")
            .body(new VndErrors.VndError(logref, message, link));
    }
}
