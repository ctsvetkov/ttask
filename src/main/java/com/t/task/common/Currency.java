package com.t.task.common;

/**
 * @author Tsvetan Tsvetkov <c.tsvetkov@gmail.com>
 */
public enum Currency {
    USD, EUR, BGN, GBP, CAD;
}