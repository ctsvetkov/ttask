package com.t.task.invoice.exception;

/**
 * @author Tsvetan Tsvetkov <c.tsvetkov@gmail.com>
 */
@SuppressWarnings("serial")
public class UnknownImageTypeException extends RuntimeException {

    public UnknownImageTypeException(final String message) {
        super(message);
    }

    public UnknownImageTypeException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
