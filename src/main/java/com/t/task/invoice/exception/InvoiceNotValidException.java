package com.t.task.invoice.exception;

import com.t.task.invoice.model.Invoice;

import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

/**
 * Exception to be thrown when validation on invoice fails.
 *
 * @author Tsvetan Tsvetkov <c.tsvetkov@gmail.com>
 */
@SuppressWarnings("serial")
public class InvoiceNotValidException extends Exception {

    private final Invoice invoice;
    private final BindingResult bindingResult;

    /**
     * Constructor for {@link InvoiceNotValidException}.
     * @param invoice the invoice that failed validation
     * @param bindingResult the results of the validation
     */
    public InvoiceNotValidException(Invoice invoice, BindingResult bindingResult) {
        this.invoice = invoice;
        this.bindingResult = bindingResult;
    }

    /**
     * Return the invoice that failed validation.
     */
    public Invoice getInvoice() {
        return this.invoice;
    }

    /**
     * Return the results of the failed validation.
     */
    public BindingResult getBindingResult() {
        return this.bindingResult;
    }

    @Override
    public String getMessage() {
        StringBuilder sb = new StringBuilder("Validation failed for invoice [")
            .append(this.invoice.toString()).append("]");

        if (this.bindingResult.getErrorCount() > 1) {
            sb.append(" with ").append(this.bindingResult.getErrorCount()).append(" errors");
        }
        
        sb.append(": ");
        for (ObjectError error : this.bindingResult.getAllErrors()) {
            sb.append("[").append(error).append("] ");
        }

        return sb.toString();
    }

}
