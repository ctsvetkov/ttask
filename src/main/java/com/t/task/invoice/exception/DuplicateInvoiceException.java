package com.t.task.invoice.exception;

/**
 * Exception to be thrown when conflict with another invoice is caught.
 *
 * @author Tsvetan Tsvetkov <c.tsvetkov@gmail.com>
 */
@SuppressWarnings("serial")
public class DuplicateInvoiceException extends RuntimeException {

    public DuplicateInvoiceException(final String invoiceNumber) {
        super(String.format("Invoice %s already exists", invoiceNumber));
    }
}
