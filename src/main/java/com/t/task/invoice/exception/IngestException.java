package com.t.task.invoice.exception;

/**
 * @author Tsvetan Tsvetkov <c.tsvetkov@gmail.com>
 */
@SuppressWarnings("serial")
public class IngestException extends RuntimeException {

    public IngestException(final String url) {
        this(url, null);
    }

    public IngestException(final String url, final Throwable cause) {
        super("Can not ingest: " + url, cause);
    }
}
