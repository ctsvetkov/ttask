package com.t.task.invoice.exception;

/**
 * @author Tsvetan Tsvetkov <c.tsvetkov@gmail.com>
 */
@SuppressWarnings("serial")
public class InvoiceNotFoundException extends RuntimeException {

    private Long invoiceId;

    public InvoiceNotFoundException(final Long invoiceId) {
        this(invoiceId, null);
    }

    public InvoiceNotFoundException(final Long invoiceId, final Throwable cause) {
        super(String.format("Could not find invoice %d", invoiceId), cause);
        this.invoiceId = invoiceId;
    }

    public Long getInvoiceId() {
        return invoiceId;
    }
}
