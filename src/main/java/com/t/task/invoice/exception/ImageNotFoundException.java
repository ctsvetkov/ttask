package com.t.task.invoice.exception;

/**
 * @author Tsvetan Tsvetkov <c.tsvetkov@gmail.com>
 */
@SuppressWarnings("serial")
public class ImageNotFoundException extends RuntimeException {

    private Long invoiceId;

    public ImageNotFoundException(final Long invoiceId) {
        this(invoiceId, null);
    }

    public ImageNotFoundException(final Long invoiceId, final Throwable cause) {
        super(String.format("Could not find image for invoice %d", invoiceId), cause);
        this.invoiceId = invoiceId;
    }

    public Long getInvoiceId() {
        return invoiceId;
    }
}
