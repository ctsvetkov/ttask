package com.t.task.invoice;

public enum Status {
    NEW, VOID, PAID;

    /**
     * Parse string value
     * @param str
     * @return if str is null or empty then return {@literal null} else {@link Status}
     */
    public static Status of(String str) {
        if (null == str || str.isEmpty()) {
            return null;
        }

        return valueOf(str.toUpperCase());
    }
}