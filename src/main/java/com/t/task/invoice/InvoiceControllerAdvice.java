package com.t.task.invoice;

import com.t.task.invoice.exception.*;

import org.springframework.hateoas.mediatype.vnderrors.VndErrors;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;
import static com.t.task.common.ControllerAdviceHelper.*;

/**
 * @author Tsvetan Tsvetkov <c.tsvetkov@gmail.com>
 */
@ControllerAdvice
class InvoiceControllerAdvice {
    @ExceptionHandler({InvoiceNotFoundException.class, ImageNotFoundException.class})
    public ResponseEntity<VndErrors.VndError> notFoundHandler(RuntimeException e) {
        return toResponseEntity(
            HttpStatus.NOT_FOUND,
            "Resource not found!",
            e.getMessage(),
            linkTo(methodOn(InvoiceController.class).all(null, null)).withRel("invoices")
        );
    }

    @ExceptionHandler(IngestException.class)
    public ResponseEntity<VndErrors.VndError> ingestExceptionHandler(IngestException e) {
        return toResponseEntity(
            HttpStatus.BAD_REQUEST,
            "Ingest failed!",
            e.getMessage(),
            linkTo(methodOn(InvoiceController.class).all(null, null)).withRel("invoices")
        );
    }
}