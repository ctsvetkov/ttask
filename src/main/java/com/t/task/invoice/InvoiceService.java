package com.t.task.invoice;

import com.t.task.invoice.model.Invoice;
import com.t.task.invoice.model.Image;

import java.util.Map;
import java.util.stream.Stream;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @author Tsvetan Tsvetkov <c.tsvetkov@gmail.com>
 */
public interface InvoiceService {
    /**
     * Returns {@link Invoice} for provided id parameter
     * @param id Invoice ID
     * @return
     * @throws InvoiceNotFoundException
     */
    Invoice findById(Long id);

    /**
     * Returns a {@link Page} of invoices meeting the parameters and paging restriction
     * provided in {@link Map} and {@code Pageable} objects.
     *
     * @param pageable
     * @return a page of worlds
     */
    Page<Invoice> findAllWithoutImage(Map<String, String> params, Pageable pageable);

    /**
     * Returns a {@link Stream} of invoices meeting the parameters restriction
     * provided in {@link Map} object.
     *
     * @param params
     * @return Stream of {@link Invoice}
     */
    Stream<Invoice> streamAll(Map<String, String> params);

    /**
     * Save invoice
     * @param invoice
     * @return
     */
    Invoice save(Invoice invoice);

    /**
     * Update invoice with the specified ID
     * @param invoice
     * @param id Invoice ID
     * @return
     */
    Invoice save(final Invoice newInvoice, final Long id);

    /**
     * Delete invoice
     * @param id Invoice ID
     * @return
     */
    void deleteById(Long id);

    /**
     * Ingest CSV data from a specified url
     * @param url resource to ingest
     * @return the number of ingested invoices
     */
    int ingest(String url);

    /**
     * Returns {@link Image} of the invoice, provided in the id parameter
     * @param id Invoice ID
     * @return
     * @throws ImageNotFoundException
     */
    Image findImage(Long id);
}