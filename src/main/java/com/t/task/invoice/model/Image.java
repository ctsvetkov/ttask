package com.t.task.invoice.model;

import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.data.relational.core.mapping.Column;

import lombok.Data;
import lombok.AllArgsConstructor;

/**
 * @author Tsvetan Tsvetkov <c.tsvetkov@gmail.com>
 */
@Data
@AllArgsConstructor
@Table("invoice_image")
public class Image {
    /**
     * Invoice ID - relation with invoice table
     */
    @Id
    private Long invoice;

    /**
     * Image mime-type
     */
    @Column("mime_type")
    @NotEmpty
    private String mimeType;

    /**
     * File name of the Image
     */
    @Column("file_name")
    @NotEmpty
    private String fileName;

    /**
     * The binary image data
     */
    @NotEmpty
    @JsonIgnore
    private byte[] data;
}