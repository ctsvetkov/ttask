package com.t.task.invoice.model;

import com.t.task.invoice.exception.UnknownImageTypeException;
import com.t.task.invoice.Status;
import com.t.task.common.Currency;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.time.LocalDate;
import java.util.Base64;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**
 * @author Tsvetan Tsvetkov <c.tsvetkov@gmail.com>
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonPropertyOrder({
    "buyer",
    "image_name",
    "invoice_image",
    "invoice_due_date",
    "invoice_number",
    "invoice_amount",
    "invoice_currency",
    "invoice_status",
    "supplier"
})
public class IOInvoice {
    /**
     * Invoice number
     */
    @JsonProperty("invoice_number")
    private String number;

    /**
     * Invoice buyer
     */
    @JsonProperty("buyer")
    private String buyer;

    /**
     * Invoice due date
     */
    @JsonProperty("invoice_due_date")
    private String dueDate;

    /**
     * Invoice amount
     */
    @JsonProperty("invoice_amount")
    private String amount;

    /**
     * Invoice amount currency
     */
    @JsonProperty("invoice_currency")
    private String currency;

    /**
     * Invoice status
     */
    @JsonProperty("invoice_status")
    private String status;

    /**
     * Invoice supplier
     */
    @JsonProperty("supplier")
    private String supplier;

    /**
     * Invoice image file name
     */
    @JsonProperty("image_name")
    private String imageName;

    /**
     * Invoice base64 encoded image data
     */
    @JsonProperty("invoice_image")
    private String imageData;

    /**
     * Factory method to create {@link IOInvoice} form {@link Invoice}
     * @param invoice
     * @return
     */
    public static IOInvoice of(Invoice invoice) {
        String imageName, imageData;

        Image image = invoice.getImage();

        if (null == image) {
            imageName = imageData = "";
        } else {
            imageName = image.getFileName();
            imageData = Base64.getEncoder()
                .encodeToString(invoice.getImage().getData());
        }

        return new IOInvoice(
            invoice.getNumber(),
            invoice.getBuyer(),
            invoice.getDueDate().toString(),
            invoice.getAmount().toString(),
            invoice.getCurrency().toString(),
            ((invoice.getStatus() == null) ? "" : invoice.getStatus().toString()),
            invoice.getSupplier(),
            imageName,
            imageData
        );
    }

    /**
     * Create native {@link Invoice} object from data
     * @return
     */
    public Invoice toInvoice() {
        return Invoice.of(
            number,
            buyer,
            LocalDate.parse(dueDate),
            Double.valueOf(amount),
            Currency.valueOf(currency.toUpperCase()),
            Status.of(status),
            supplier,
            hashCode(),
            toImage()
        );
    }

    /**
     * Create native {@link Image} object from data
     * @return
     */
    public Image toImage() {
        if (null == imageName || imageName.isEmpty()) {
            return null;
        }

        if (null == imageData || imageData.isEmpty()) {
            return null;
        }

        String mimeType = null;
        byte[] data = Base64.getDecoder().decode(imageData);

        try (InputStream is = new ByteArrayInputStream(data)) {
            mimeType = URLConnection.guessContentTypeFromStream(is);
        } catch (IOException e) {

        }

        if (null == mimeType || mimeType.isEmpty()) {
            mimeType = URLConnection.guessContentTypeFromName(imageName);
        }

        if (null == mimeType || mimeType.isEmpty()) {
            throw new UnknownImageTypeException("Cannot find invoice image mime-type.");
        }

        return new Image(null, mimeType, imageName, data);
    }
}