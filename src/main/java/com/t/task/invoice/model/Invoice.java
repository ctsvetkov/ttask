package com.t.task.invoice.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.data.relational.core.mapping.Column;

import com.t.task.invoice.Status;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.t.task.common.Currency;

import java.time.LocalDate;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.AllArgsConstructor;

/**
 * @author Tsvetan Tsvetkov <c.tsvetkov@gmail.com>
 */
@Data
@AllArgsConstructor
@Table("invoice")
public class Invoice {
    /**
     * Invoice ID
     */
    @Id
    private final Long id;

    /**
     * Invoice number
     */
    @NotEmpty
    private final String number;

    /**
     * Invoice buyer
     */
    @NotEmpty
    private final String buyer;

    /**
     * Invoice due date
     */
    @Column("due_date")
    @NotNull
    private final LocalDate dueDate;

    /**
     * Invoice amount
     */
    @NotNull
    private final Double amount;

    /**
     * Invoice amount currency
     */
    @NotNull
    private final Currency currency;

    /**
     * Invoice status
     */
    private final Status status;

    /**
     * Invoice supplier
     */
    @NotNull
    private final String supplier;

    /**
     * Invoice input unique hash code,
     * used to prevent double row insertion
     */
    @Column("ingest_hash")
    @NotNull
    private final Integer ingestHash;

    /**
     * Invoice images
     */
    @JsonInclude(Include.NON_NULL)
    private final Image image;

    public static Invoice of(
        final String number,
        final String buyer,
        final LocalDate dueDate,
        final Double amount,
        final Currency currency,
        final Status status,
        final String supplier
    ) {
        return of(number, buyer, dueDate, amount, currency, status, supplier);
    }

    public static Invoice of(
        final String number,
        final String buyer,
        final LocalDate dueDate,
        final Double amount,
        final Currency currency,
        final Status status,
        final String supplier,
        final Integer ingestHash
    ) {
        return of(number, buyer, dueDate, amount, currency, status, supplier, ingestHash, null);
    }

    public static Invoice of(
        final String number,
        final String buyer,
        final LocalDate dueDate,
        final Double amount,
        final Currency currency,
        final Status status,
        final String supplier,
        final Integer ingestHash,
        final Image image
    ) {
        return new Invoice(null, number, buyer, dueDate, amount, currency, status, supplier, ingestHash, image);
    }

    public static Invoice of(
        final Long id,
        final String number,
        final String buyer,
        final LocalDate dueDate,
        final Double amount,
        final Currency currency,
        final Status status,
        final String supplier
    ) {
        return new Invoice(id, number, buyer, dueDate, amount, currency, status, supplier, null, null);
    }

    public static Invoice of(
        final Long id,
        final String number,
        final String buyer,
        final LocalDate dueDate,
        final Double amount,
        final Currency currency,
        final Status status,
        final String supplier,
        final Integer ingestHash
    ) {
        return new Invoice(id, number, buyer, dueDate, amount, currency, status, supplier, ingestHash, null);
    }

    public Invoice withId(final Long id) {
        return new Invoice(id, number, buyer, dueDate, amount, currency, status, supplier, ingestHash, image);
    }
}