package com.t.task.invoice;

import com.t.task.invoice.model.*;
import com.t.task.invoice.repository.ImageRepository;
import com.t.task.invoice.repository.InvoiceRepository;
import com.t.task.invoice.exception.*;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import java.net.HttpURLConnection;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;
import org.springframework.validation.BindException;

import lombok.extern.slf4j.Slf4j;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

/**
 * @author Tsvetan Tsvetkov <c.tsvetkov@gmail.com>
 */
@Service
@Slf4j
public class InvoiceServiceImpl implements InvoiceService {

    private final InvoiceRepository invoiceRepository;
    private final ImageRepository imageRepository;
    private final Validator validator;

    /**
     * Used to convert Page to Stream
     */
    private Page<Invoice> invoicePage;

    @Autowired
    InvoiceServiceImpl(InvoiceRepository invoiceRepository, ImageRepository imageRepository, Validator validator) {
        this.invoiceRepository = invoiceRepository;
        this.imageRepository = imageRepository;
        this.validator = validator;
    }

    @Override
    public Invoice findById(final Long id) {
        return invoiceRepository.findById(id).orElseThrow(() -> new InvoiceNotFoundException(id));
    }

    @Override
    public Page<Invoice> findAllWithoutImage(Map<String, String> params, Pageable pageable) {
        return invoiceRepository.findAll(params, pageable, false);
    }

    @Override
    public Stream<Invoice> streamAll(Map<String, String> params) {
        final int PAGE_SIZE = 10;

        invoicePage = invoiceRepository.findAll(params, PageRequest.of(0, PAGE_SIZE));

        Spliterator<Invoice> spliterator = Spliterators.emptySpliterator();
        if (invoicePage.isEmpty()) {
            return StreamSupport.stream(spliterator, false);
        }

        spliterator = Spliterators.spliterator(new Iterator<Invoice>() {
            private int page = 1;
            private int item = 0;

            @Override
            public boolean hasNext() {
                if (item < invoicePage.getContent().size()) {
                    return true;
                }
                return invoicePage.hasNext();
            }

            @Override
            public Invoice next() {
                if (item >= invoicePage.getContent().size() && invoicePage.hasNext()) {
                    invoicePage = invoiceRepository.findAll(params, PageRequest.of(page, PAGE_SIZE));
                    page++;
                    item = 0;
                }

                Invoice invoice = invoicePage.getContent().get(item);
                item++;

                return invoice;
            }
        }, invoicePage.getTotalElements(), Spliterator.IMMUTABLE);

        return StreamSupport.stream(spliterator, false);
    }

    @Override
    public Invoice save(final Invoice invoice) {
        return invoiceRepository.save(invoice);
    }

    @Override
    public Invoice save(final Invoice newInvoice, final Long id) {
        Invoice invoice = this.findById(id);
        return this.save(Invoice.of(invoice.getId(), newInvoice.getNumber(), newInvoice.getBuyer(),
                newInvoice.getDueDate(), newInvoice.getAmount(), newInvoice.getCurrency(), newInvoice.getStatus(),
                newInvoice.getSupplier()));
    }

    @Override
    public void deleteById(final Long id) {
        invoiceRepository.deleteById(id);
    }

    @Override
    public int ingest(String url) {
        int count = 0;
        try (BufferedInputStream in = openHttpStream(url)) {
            CsvSchema bootstrapSchema = CsvSchema.emptySchema().withHeader();
            CsvMapper mapper = new CsvMapper();
            MappingIterator<IOInvoice> reader = mapper.readerFor(IOInvoice.class).with(bootstrapSchema).readValues(in);
            while (reader.hasNext()) {
                if (save(reader.next())) {
                    count++;
                }
            }
        } catch (IOException e) {
            log.error("Can not ingest: " + url, e);
            throw new IngestException(url, e);
        }

        return count;
    }

    @Override
    public Image findImage(final Long id) {
        return imageRepository.findById(id)
            .orElseThrow(() -> new ImageNotFoundException(id));
    }

    /**
     * Open HTTP download stream and handle redirect 3xx
     *
     * @param url 
     * @return input stream
     */
    private BufferedInputStream openHttpStream(String url) throws IOException {
        HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();

        int redirectCount = 0;
        while (redirectCount < 5) {
            // detect, 3xx is redirect
            int status = conn.getResponseCode();
            if (HttpURLConnection.HTTP_OK == status) {
                break;
            }

            if (
                status == HttpURLConnection.HTTP_MOVED_TEMP
                || status == HttpURLConnection.HTTP_MOVED_PERM
                || status == HttpURLConnection.HTTP_SEE_OTHER
            ) {
                // get redirect url from "location" header field
                String newUrl = conn.getHeaderField("Location");

                // get the cookie if need, for login
                String cookies = conn.getHeaderField("Set-Cookie");

                // open the new connection again
                conn = (HttpURLConnection) new URL(newUrl).openConnection();
                conn.setRequestProperty("Cookie", cookies);
            }

            redirectCount++;
        }

        return new BufferedInputStream(conn.getInputStream());
    }

    @Transactional
    private boolean save(IOInvoice ioInvoice) {
        try {
            Invoice invoice = ioInvoice.toInvoice();

            this.validate(invoice);

            // Check whether the invoice is already ingested
            if (invoiceRepository.existsByNumberAndHash(invoice.getNumber(), invoice.getIngestHash())) {
                throw new DuplicateInvoiceException(invoice.getNumber());
            }

            invoice = invoiceRepository.save(invoice);
        } catch (InvoiceNotValidException e) {
            log.error("Invalid invoice data for " + ioInvoice.getNumber(), e);
            return false;
        } catch (DuplicateInvoiceException e) {
            log.info(e.getMessage());
            return false;
        } catch (Exception e) {
            log.warn("Can not ingest invoice: " + ioInvoice.getNumber(), e);
            return false;
        }

        return true;
    }

    /**
     * Trigger JSR 380: Bean Validation
     * @param invoice
     * @throws InvoiceNotValidException
     */
    public void validate(Invoice invoice) throws InvoiceNotValidException {
        BindException result = new BindException(invoice, "Invoice");
        validator.validate(invoice, result);
        if (result.hasErrors()) {
            throw new InvoiceNotValidException(invoice, result);
        }
    }
}
