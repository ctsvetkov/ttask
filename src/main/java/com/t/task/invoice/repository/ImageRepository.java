package com.t.task.invoice.repository;

import com.t.task.invoice.model.Image;

import org.springframework.data.repository.CrudRepository;

/**
 * @author Tsvetan Tsvetkov <c.tsvetkov@gmail.com>
 */
public interface ImageRepository extends CrudRepository<Image, Long> {

}