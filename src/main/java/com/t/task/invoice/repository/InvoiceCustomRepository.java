package com.t.task.invoice.repository;

import java.util.Map;

import com.t.task.invoice.model.Invoice;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

/**
 * @author Tsvetan Tsvetkov <c.tsvetkov@gmail.com>
 */
interface InvoiceCustomRepository {
    /**
     * Returns all invoices sorted by the given options.
     *
     * @param sort
     * @return all invoices sorted by the given options
     * @throws IllegalArgumentException if sort is {@literal null}.
     */
    Iterable<Invoice> findAll(Sort sort);

    /**
     * Returns all invoices sorted by the given options.
     *
     * @param sort
     * @param withImage whether to hydrate the image relation
     * @return all invoices sorted by the given options
     * @throws IllegalArgumentException if sort or withImage is {@literal null}.
     */
    Iterable<Invoice> findAll(Sort sort, Boolean withImage);

    /**
     * Returns a {@link Page} of invoices meeting the paging restriction provided in the {@code Pageable} object.
     *
     * @param pageable
     * @return a page of invoices
     * @throws IllegalArgumentException if pageable is {@literal null}.
     */
    Page<Invoice> findAll(Pageable pageable);

    /**
     * Returns a {@link Page} of invoices meeting the paging restriction provided in the {@code Pageable} object.
     *
     * @param pageable
     * @param withImage whether to hydrate the image relation
     * @return a page of invoices
     * @throws IllegalArgumentException if pageable is {@literal null}.
     */
    Page<Invoice> findAll(Pageable pageable, Boolean withImage);

    /**
     * Returns a {@link Page} of invoices meeting the paging restriction provided in the {@code Pageable} object.
     *
     * @param params filter params
     * @param pageable
     * @return a page of invoices
     * @throws IllegalArgumentException if params or pageable is {@literal null}.
     */
    Page<Invoice> findAll(Map<String, String> params, Pageable pageable);

    /**
     * Returns a {@link Page} of invoices meeting the paging restriction provided in the {@code Pageable} object.
     *
     * @param params filter params
     * @param pageable
     * @param withImage whether to hydrate the image relation
     * @return a page of invoices
     * @throws IllegalArgumentException if params, pageable or withImage is {@literal null}.
     */
    Page<Invoice> findAll(Map<String, String> params, Pageable pageable, Boolean withImage);
}