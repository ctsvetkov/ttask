package com.t.task.invoice.repository;

import com.t.task.invoice.model.Invoice;

import java.util.Optional;

import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.lang.NonNull;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Tsvetan Tsvetkov <c.tsvetkov@gmail.com>
 */
public interface InvoiceRepository extends CrudRepository<Invoice, Long>, InvoiceCustomRepository {
    /**
     * Returns whether an invoice with the given number and ingest hash code exists.
     *
     * @param number must not be {@literal null}.
     * @param ingestHash must not be {@literal null}.
     * @return {@literal true} if an invoice exists, {@literal false} otherwise.
     * @throws IllegalArgumentException if number or ingestHash is {@literal null}.
     */
    @Query("SELECT EXISTS(SELECT NULL FROM invoice WHERE number = :number AND ingest_hash = :hash)")
    boolean existsByNumberAndHash(@NonNull @Param("number") String number, @NonNull @Param("hash") Integer ingestHash);

    @Query("SELECT * FROM invoice WHERE number = :number")
    Optional<Invoice> findByNumber(@Param("number") String number);
  
    @Query("SELECT * FROM invoice WHERE number LIKE :number")
    Iterable<Invoice> findByNumberLike(@Param("number") String number);
}