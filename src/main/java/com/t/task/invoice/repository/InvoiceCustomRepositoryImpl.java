package com.t.task.invoice.repository;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringJoiner;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.domain.PageImpl;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import com.t.task.common.Currency;
import com.t.task.invoice.Status;
import com.t.task.invoice.model.Image;
import com.t.task.invoice.model.Invoice;

/**
 * @author Tsvetan Tsvetkov <c.tsvetkov@gmail.com>
 */
@Repository
class InvoiceCustomRepositoryImpl implements InvoiceCustomRepository {
    public static final String SELECT_QUERY_WITH_IMAGE = "SELECT invoice.*, img.data AS img_data, img.invoice AS img_invoice, img.file_name AS img_file_name, img.mime_type AS img_mime_type FROM invoice LEFT JOIN invoice_image AS img ON img.invoice = invoice.id";
    public static final String COUNT_QUERY_WITH_IMAGE = "SELECT count(invoice.id) FROM invoice LEFT JOIN invoice_image AS img ON img.invoice = invoice.id";
    public static final String SELECT_QUERY = "SELECT * FROM invoice";
    public static final String COUNT_QUERY = "SELECT count(id) FROM invoice";
    public static Set<String> searchParams;

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    static {
        searchParams = new HashSet<String>(Arrays.asList("number", "buyer", "supplier", "status"));
    }

    @Autowired
    InvoiceCustomRepositoryImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public Iterable<Invoice> findAll(Sort sort) {
        return this.findAll(sort, true);
    }

    @Override
    public Iterable<Invoice> findAll(Sort sort, Boolean withImage) {
        Assert.notNull(sort, "Parameter sort must not be null");
        Assert.notNull(withImage, "Parameter withImage must not be null");

        StringBuilder query = new StringBuilder();
        RowMapper<Invoice> rowMapper;

        if (withImage) {
            query.append(SELECT_QUERY_WITH_IMAGE);
            rowMapper = this::mapRowWithImage;
        } else {
            query.append(SELECT_QUERY);
            rowMapper = this::mapRow;
        }

        query.append(builOrder(sort));

        JdbcTemplate jdbcTemplate = namedParameterJdbcTemplate.getJdbcTemplate();
        return jdbcTemplate.query(
            query.toString(),
            rowMapper
        );
    }

    @Override
    public Page<Invoice> findAll(Pageable pageable) {
        return this.findAll(pageable, true);
    }

    @Override
    public Page<Invoice> findAll(Pageable pageable, Boolean withImage) {
        Map<String, String> hMap = new HashMap<String, String>();
        return this.findAll(hMap, pageable, withImage);
    }

    @Override
    public Page<Invoice> findAll(Map<String, String> params, Pageable pageable) {
        return this.findAll(params, pageable, true);
    }

    @Override
    public Page<Invoice> findAll(Map<String, String> params, Pageable pageable, Boolean withImage) {
        Assert.notNull(params, "Parameter params must not be null");
        Assert.notNull(pageable, "Parameter pageable must not be null");
        Assert.notNull(withImage, "Parameter withImage must not be null");

        StringBuilder query = new StringBuilder();
        StringBuilder countQuery = new StringBuilder();
        RowMapper<Invoice> rowMapper;

        if (withImage) {
            query.append(SELECT_QUERY_WITH_IMAGE);
            countQuery.append(COUNT_QUERY_WITH_IMAGE);
            rowMapper = this::mapRowWithImage;
        } else {
            query.append(SELECT_QUERY);
            countQuery.append(COUNT_QUERY);
            rowMapper = this::mapRow;
        }

        long total = 0;
        List<Invoice> data = null;

        if (params.size() > 0 && buildQueryFromMap(params, query, countQuery)) {
            SqlParameterSource sqlParams = new MapSqlParameterSource(params);
            total = namedParameterJdbcTemplate.queryForObject(
                countQuery.toString(),
                sqlParams,
                Integer.class
            );

            data = namedParameterJdbcTemplate.query(
                query.append(buildPageable(pageable)).toString(),
                sqlParams,
                rowMapper
            );
        } else {
            JdbcTemplate jdbcTemplate = namedParameterJdbcTemplate.getJdbcTemplate();
            total = jdbcTemplate.queryForObject(
                countQuery.toString(),
                new Object[] {},
                Integer.class
            );

            data = jdbcTemplate.query(
                query.append(buildPageable(pageable)).toString(),
                rowMapper
            );
        }

        return new PageImpl<Invoice>(data, pageable, total);
    }

    private String buildPageable(Pageable pageable) {
        return builOrder(pageable.getSort())
            + " LIMIT " + pageable.getPageSize()
            + " OFFSET " + pageable.getOffset();
    }

    private String builOrder(Sort sort) {
        if (sort.isEmpty()) {
            return " ";
        }

        StringJoiner sj = new StringJoiner(",");

        for (Order o: sort) {
            sj.add(o.getProperty() + " " + o.getDirection().toString());
        }

        return " ORDER BY " + sj;
    }

    private Invoice mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new Invoice(
            rs.getLong("id"),
            rs.getString("number"),
            rs.getString("buyer"),
            LocalDate.parse(rs.getString("due_date")),
            rs.getDouble("amount"),
            Currency.valueOf(rs.getString("currency")),
            Status.of(rs.getString("status")),
            rs.getString("supplier"),
            rs.getInt("ingest_hash"),
            null
        );
    }

    private Invoice mapRowWithImage(ResultSet rs, int rowNum) throws SQLException {
        Image image = null;

        if (rs.getLong("img_invoice") != 0) {
            image = new Image(
                rs.getLong("img_invoice"),
                rs.getString("img_mime_type"),
                rs.getString("img_file_name"),
                rs.getBytes("img_data")
            );
        }

        return new Invoice(
            rs.getLong("id"),
            rs.getString("number"),
            rs.getString("buyer"),
            LocalDate.parse(rs.getString("due_date")),
            rs.getDouble("amount"),
            Currency.valueOf(rs.getString("currency")),
            Status.of(rs.getString("status")),
            rs.getString("supplier"),
            rs.getInt("ingest_hash"),
            image
        );
    }

    /**
     * Build SQL WHERE clause from {@link Map} parameters
     * 
     * @param query
     * @param countQuery
     * @return
     */
    private boolean buildQueryFromMap(Map<String, String> params, StringBuilder query, StringBuilder countQuery) {
        StringJoiner sj = new StringJoiner(" AND ", " WHERE ", " ");
        boolean hasParams = false;

        for (String param: params.keySet()) {
            if (searchParams.contains(param)) {
                hasParams = true;
                sj.add(String.format("%s = :%s", param, param));
            }
        }

        if (hasParams) {
            String append = sj.toString();
            query.append(append);
            countQuery.append(append);
        }

        return hasParams;
    }
}