package com.t.task.invoice;

import com.t.task.invoice.model.Invoice;
import com.t.task.invoice.model.IOInvoice;
import com.t.task.invoice.model.Image;

import com.fasterxml.jackson.databind.SequenceWriter;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.hateoas.PagedModel;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletResponse;


/**
 * @author Tsvetan Tsvetkov <c.tsvetkov@gmail.com>
 */
@RestController
@Slf4j
@RequestMapping(value = "/api/invoices", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
public class InvoiceController {

    private final InvoiceService invoiceService;
    private final InvoiceModelAssembler invoiceAssembler;
    private final PagedResourcesAssembler<Invoice> pagedResourcesAssembler;

    @Autowired
    InvoiceController(
        InvoiceService invoiceService,
        InvoiceModelAssembler invoiceAssembler,
        PagedResourcesAssembler<Invoice> pagedResourcesAssembler
    ) {
        this.invoiceService = invoiceService;
        this.invoiceAssembler = invoiceAssembler;
        this.pagedResourcesAssembler = pagedResourcesAssembler;
    }

    @GetMapping
    public PagedModel<EntityModel<Invoice>> all(@RequestParam Map<String, String> params, Pageable pageable) {
        return pagedResourcesAssembler.toModel(invoiceService.findAllWithoutImage(params, pageable), invoiceAssembler);
    }

    @GetMapping("/csv")
    public void allCsv(
        HttpServletResponse response,
        @RequestParam Map<String, String> params
    ) throws IOException {
        response.setContentType("application/csv");
        response.setHeader("Content-Disposition", "attachment; filename=\"invoices.csv\"");

        PrintWriter printWriter = response.getWriter();

        CsvMapper mapper = new CsvMapper();
        CsvSchema schema = mapper.schemaFor(IOInvoice.class).withHeader();
        SequenceWriter writer = mapper.writer(schema).writeValues(printWriter);
        
        invoiceService.streamAll(params).forEach(invoice -> {
            try {
                writer.write(IOInvoice.of(invoice));
            } catch (IOException e) {
                log.error(String.format("Can't export invoice: %d", invoice.getId()), e);
            }
        });
    }

    @GetMapping("/{id}")
    public EntityModel<Invoice> one(@PathVariable Long id) {
        return invoiceAssembler.toModel(invoiceService.findById(id));
    }

    /**
     * Return the invoice image
     * @param id Invoice ID
     * @return
     */
    @GetMapping("/{id}/image")
    public ResponseEntity<byte[]> image(@PathVariable Long id) {
        Image image = invoiceService.findImage(id);
        return ResponseEntity
            .ok()
            .header("content-type", image.getMimeType())
            .header("content-disposition", String.format("inline; filename=\"%s\"", image.getFileName()))
            .body(image.getData());
    }

    /**
     * ResponseEntity is used to create an *HTTP 201 Created* status message.
     * This type of response typically includes a Location response header,
     * and we use the URI derived from the model’s self-related link.
     *
     * @param invoice
     * @return
     */
    @PostMapping
    public ResponseEntity<?> createInvoice(@RequestBody final Invoice invoice) {
        EntityModel<Invoice> entityModel = invoiceAssembler.toModel(invoiceService.save(invoice));

        return ResponseEntity
            .created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri())
            .body(entityModel);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateInvoice(@RequestBody final Invoice invoice, @PathVariable Long id) {
        EntityModel<Invoice> entityModel = invoiceAssembler.toModel(invoiceService.save(invoice, id));

        return ResponseEntity
          .created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri())
          .body(entityModel);
    }

    @DeleteMapping("/{id}")
    ResponseEntity<Void> deleteInvoice(@PathVariable final Long id) {
        invoiceService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/ingest")
    public EntityModel<String> ingest(@RequestParam("url") final String url) {
        int count = invoiceService.ingest(url);

        return new EntityModel<String>(
            String.format("{\"Ingested\": %d}", count),
            linkTo(methodOn(InvoiceController.class).all(null, null)).withRel("invoices")
        );
    }
}