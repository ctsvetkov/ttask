package com.t.task.invoice;

import com.t.task.invoice.model.Invoice;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

/**
 * HATEOAS Invoice representation model assembler
 * @author Tsvetan Tsvetkov <c.tsvetkov@gmail.com>
 */
@Component
class InvoiceModelAssembler implements RepresentationModelAssembler<Invoice, EntityModel<Invoice>> {

    /**
     * Add JSON HAL links to the Invoice entity
     *
     * @param invoice
     * return Hateoas EntityModel
     */
    @Override
    public EntityModel<Invoice> toModel(Invoice invoice) {
        return new EntityModel<Invoice>(invoice,
            linkTo(methodOn(InvoiceController.class).one(invoice.getId())).withSelfRel(),
            linkTo(methodOn(InvoiceController.class).all(null, null)).withRel("invoices"),
            linkTo(methodOn(InvoiceController.class).image(invoice.getId())).withRel("image")
        );
    }

    @Override
    public CollectionModel<EntityModel<Invoice>> toCollectionModel(Iterable<? extends Invoice> entities) {
        CollectionModel<EntityModel<Invoice>> collectionModel = StreamSupport.stream(entities.spliterator(), false)
            .map(this::toModel)
            .collect(Collectors.collectingAndThen(Collectors.toList(), CollectionModel::new));

        return collectionModel.add(linkTo(methodOn(InvoiceController.class).all(null, null)).withSelfRel());
    }
}
