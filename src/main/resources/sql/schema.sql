-- invoice table
DROP TABLE IF EXISTS invoice;
 
CREATE TABLE invoice (
  `id` IDENTITY NOT NULL PRIMARY KEY,
  `number` VARCHAR(60) NOT NUll,
  `buyer` VARCHAR(250) NOT NULL,
  `due_date` DATE NOT NULL,
  `amount` DOUBLE NOT NULL,
  `currency` CHAR(3) NOT NULL,
  `status` CHAR(4),
  `supplier` VARCHAR(250) NOT NULL,
  `ingest_hash` INT NOT NULL
);

-- invoice_image table
DROP TABLE IF EXISTS invoice_image;

CREATE TABLE invoice_image (
  `invoice` BIGINT NOT NULL PRIMARY KEY, -- Invoice ID
  `mime_type` VARCHAR(60) NOT NULL,
  `file_name` VARCHAR(250) NOT NULL,
  `data` BINARY(6M) NOT NULL
);
